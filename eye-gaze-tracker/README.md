# RCL Eye Gaze Tracker

This is an eye gaze tracking solution designed for the surgeon console of the da Vinci surgical robot platforms. This eye gaze tracker determines where a surgeon is looking within the surgical scene.

The software provided here open-source is the UI front-end component of the eye gaze tracker software. A license must be purchased from the UBC RCL for access to the gaze tracking libraries which are needed in order to run the eye gaze tracker.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Install Qt, Qt Creator, Visual Studio 2015. You can download Qt from https://www.qt.io/download.

You may replace these library files with updated versions of each module. However, note that on Windows we have used the MSVC2015 64-bit compiler and on Ubuntu we use GCC, and we have been developing with the following versions:

* Ffmpeg 4.0.2
* OpenCV 3.3
* Boost 1.66
* Qt 5.9.3

### Setup

1.) Download or clone this repository.  The library contains everything that you need to compile.
2.) Open up gazeTrackGUI.pro
3.) Select Desktop QT 5.12.2 MSVC2015 64 bit or whatever version you have
4.) Build

#### Windows

#### Ubuntu


### Deployment

You should now open the Qt project (src/gazeTrackGUI_release.pro) in Qt Creator. Set the project to use the appropriate compiler and build the project in Release mode. The output of the build will be contained in the build/release folder. The output executable is named gazeTrackGUI.

## Authors

* Irene Tong
* Maxwell Li

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the ___ License - see the [LICENSE.md](LICENSE.md) file for details