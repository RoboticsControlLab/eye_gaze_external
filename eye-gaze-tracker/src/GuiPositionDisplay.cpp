/*
GuiPositionDisplay.cpp

Author(s): Irene Tong, Maxwell Li
Created on: August 1, 2018

(c) Copyright 2018 University of British Columbia

--- begin license - do not edit ---

    This file is part of CGaze UI. 
   
    CGaze UI is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Cgaze UI is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Cgaze UI.  If not, see <https://www.gnu.org/licenses/>.
--- end license ---
*/




#include "GuiPositionDisplay.h"

/*!
 * \brief GazePositionDisplay::GazePositionDisplay This dialog shows the current position of eye gaze to the researcher.
 * \param tracker Eye gaze tracker class
 */
GazePositionDisplay::GazePositionDisplay(EyeTracker* tracker)
{
    _p_tracker = tracker;
    //this->setWindowState(Qt::WindowFullScreen);
    this->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMinMaxButtonsHint);
    // Setup background colour
    this->setAutoFillBackground(true);
    QPalette pal = this->palette();
    pal.setColor(this->backgroundRole(),QColor(160,160,160,255));
    this->setPalette(pal);
    //this->resize(640,480);

    _timer = new QTimer(this);

    connect(_timer, SIGNAL(timeout()), this, SLOT(update()));
    _timer->start(30);
}

GazePositionDisplay::~GazePositionDisplay(void)
{
}

void GazePositionDisplay::update()
{
    repaint();
}

void GazePositionDisplay::paintEvent(QPaintEvent *event)
{
	(void)event;

    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing);

    // Draw test background
    p.setPen(Qt::NoPen);

    float border = this->height() / 5;
    float dx = (this->width()-2*border)/2;
    float dy = (this->height()-2*border)/2;
    int numRows = 3;
    int numCols = 3;
    for ( int i = 0 ; i < numRows ; i++ )
    {
        for ( int j = 0 ; j < numCols ; j++ )
        {
            QPoint newPoint(int(border+i*dx), int(border+j*dy));
            p.setBrush(Qt::lightGray);
            p.drawEllipse(newPoint, 50, 50);
            p.setBrush(Qt::darkGray);
            p.drawEllipse(newPoint, 2, 2);
        }
    }
    // Draw gazepoint
    cv::Point2f pogR;
    cv::Point2f pogL;
    cv::Point2f pogB;
    bool rightEyeGood = _p_tracker->getPOG(pogR, rclgaze::RIGHT_EYE);
    bool leftEyeGood =  _p_tracker->getPOG(pogL, rclgaze::LEFT_EYE);
    bool combEyeGood = _p_tracker->getPOG(pogB, rclgaze::BOTH_EYES);

    float pXR = 0;
    float pYR = 0;
    float pXL = 0;
    float pYL = 0;
    float pXB = 0;
    float pYB = 0;
    float numEyes = 0;
    if (rightEyeGood)
    {
        pXR = this->width() * (pogR.x / 100.0f);
        pYR = this->height() * (pogR.y / 100.0f);
        numEyes += 1;
    }
    if (leftEyeGood)
    {
        pXL = this->width() * (pogL.x / 100.0f);
        pYL = this->height() * (pogL.y / 100.0f);
        numEyes += 1;
    }
    if (combEyeGood)
    {
        pXB = this->width() * (pogB.x / 100.0f);
        pYB = this->height() * (pogB.y / 100.0f);
        numEyes += 1;
    }

    int SMALL_RADIUS = 10;
    p.setPen(Qt::NoPen);

    p.setBrush(Qt::blue);
    p.drawEllipse(QPointF(qreal(pXR),qreal(pYR)),SMALL_RADIUS,SMALL_RADIUS);

    p.setBrush(Qt::red);
    p.drawEllipse(QPointF(qreal(pXL),qreal(pYL)),SMALL_RADIUS,SMALL_RADIUS);

    p.setBrush(Qt::magenta);
    p.drawEllipse(QPointF(qreal(pXB), qreal(pYB)), SMALL_RADIUS, SMALL_RADIUS);
    qDebug() << pXB << pYB << pogL.x << pogB.x << combEyeGood;
}
