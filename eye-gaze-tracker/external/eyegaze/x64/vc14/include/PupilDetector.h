#ifndef PUPIL_DETECTOR_H
#define PUPIL_DETECTOR_H
#include "stdafx.h"
/*!
  @brief Functions used for detecting the pupil in an image
*/

// TODO: Move to feature detector class??

// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

// Std
#include <math.h>
#include <ctime>
#include <iostream>

// Gaze
#include "EyeGazeCommon.h"
#include "UtilImageProcessing.h"
#include "haar_eye_localization.h"

#include "PupilStarburst.h"
#include "PupilRansac.h"
#include "GuiDebugWindow.h"

// Boost
#include "boost/timer/timer.hpp"

// Debug
#include "DebugSetting.h"

/*!
 * \brief Find the center of the pupil
 *
 * Uses integral projection function, starburst, and RANSAC algorithms to find the pupil.
 * The pupil is stored as an ellipse in a cv::RotatedRect variable.
 *
 * \param src Eye image
 * \param prevPupil Previous known pupil
 * \param newPupil Return value, detected pupil
 * \param prevPupilValid True if previous frame had a pupil
 * \param haarClassifier Haar classifier for eye detection
 * \param side Either LEFT_EYE or RIGHT_EYE
 * \param debug_count For debugging...
 * \param pupilParameter Pupil details (mean, stdev)
 * \param pupil_thres Pupil intensity threshold
 * \return Returns true if pupil center is found
 */
bool find_pupil_center(cv::Mat src, \
                       cv::RotatedRect prevPupil, \
                       cv::RotatedRect& newPupil, \
                       bool prevPupilValid, \
                       cv::CascadeClassifier haarClassifier, \
                       rclgaze::eyeSide side,\
                       std::vector<cv::RotatedRect> ref_pupil_list, \
                       std::vector<float>& pupil_thres);

///*!
// * \brief Finds the rough pupil location using gradient eye localisation or Haar
// * \param src Image
// * \param prevPupilCentre Previously known pupil center for reference
// * \param pupilCentre Return value of pupil location
// * \param isLocalized Set as true if ROI is being used
// * \return Returns true if rough eye center is found
// */
//bool find_rough_eye_location(cv::Mat src, cv::Point2f prevPupilCentre, cv::Point2f& pupilCentre, bool isLocalized);

void calc_pupil_meanStdDev(std::vector<cv::RotatedRect> pupil,float& width_mean, float& width_stdev, float& height_mean, float& height_stdev);

/*!
 * \brief Save the image if pupil detection failed.
 * \param src Eye image
 * \param prevPupil Previous known pupil
 * \param prevPupilValid True if previous frame had a pupil
 * \param haarClassifier HAAR classifier used for eye detection
 */
void saveFailedPupil(cv::Mat src, \
                     cv::RotatedRect prevPupil,\
                     bool prevPupilValid, \
                     cv::CascadeClassifier haarClassifier);
#endif 
