#ifndef UTILGEOMETRY_H
#define UTILGEOMETRY_H
#include "stdafx.h"
#include <opencv2/core.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/operation.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/triangular.hpp>

using namespace boost::numeric;

namespace rclgaze {
	/*!
	 @brief Geometry-related utility functions.
	 */

	 /*!
	  * \brief Calculates a set of points along the perimeter of ellipse
	  * \param ellipse OpenCV ellipse
	  * \param numPoints Number of points to generate
	  * \param retX The x-coordinates of generated perimeter points
	  * \param retY The y-coordinates of generated perimeter points
	  * \return Returns true if sucessful
	  */
	void generate_ellipse_points(cv::RotatedRect ellipse, int numPoints, ublas::vector<float>& retX, ublas::vector<float>& retY);
}
#endif // UTILGEOMETRY_H
