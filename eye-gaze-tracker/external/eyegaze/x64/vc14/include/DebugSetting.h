#ifndef DEBUG_SETTING_H
#define DEBUG_SETTING_h

#include "EyeDetectorConstants.h"
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

// Debug constants, set to false when not needed
#define DEBUG_GLINT false
#define DEBUG_PUPIL false

#if (DEBUG_GLINT == true)
// these variables are created to help to scale the value to integers, since trackbar does not accept float
extern int glint_roi_width_scaler_2_value;
extern int glint_roi_height_scaler_2_value;
extern int glint_match_single_translation_max_value;
extern int glint_match_mul_translation_max_value;

void init_glint_debug_windows(void);

extern void glint_roi_width_scalar_2_callback(int, void*); 
extern void glint_roi_height_scalar_2_callback(int, void*);
extern void glint_match_single_translation_max_callback(int, void*);
extern void glint_match_mul_translation_max_callback(int, void*);
#endif 

#if (DEBUG_PUPIL == true)
// these variables are created to help to scale the value to integers, since trackbar does not accept float
extern int pupil_mean_lo_thres_value;
extern int pupil_mean_hi_thres_value;
extern int ray_len_scalar_value;
extern int pupil_ratio_max_value;
extern int pupil_ratio_min_value;

void init_pupil_debug_windows(void);

void pupil_mean_lo_thres_value_callback(int, void*);
void pupil_mean_hi_thres_value_callback(int, void*);
void ray_len_scalar_value_callback(int, void*);
void pupil_ratio_max_value_callback(int, void*);
void pupil_ratio_min_value_callback(int, void*);
#endif 



#endif // !DEBUG_SETTING_H

