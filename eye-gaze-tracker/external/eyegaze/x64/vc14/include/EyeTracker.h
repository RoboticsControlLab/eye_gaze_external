#ifndef EYEGAZETRACKER_H
#define EYEGAZETRACKER_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "EyeGazeCommon.h"
#include <boost/asio.hpp>
#include <boost/function.hpp>
#include <boost/numeric/ublas/matrix.hpp>

#if _WINDOWS
#ifdef EYEGAZETRACKER_DLL_EXPORTS
#define EYEGAZETRACKER_API __declspec(dllexport)   
#else  
#define EYEGAZETRACKER_API __declspec(dllimport)   
#endif
#define EYEGAZETRACKER_API_LOCAL
#else
#if __GNUC__ >= 4
#define EYEGAZETRACKER_API __attribute__ ((visibility ("default")))
#define EYEGAZETRACKER_API_LOCAL __attribute__ ((visibility ("hidden")))
#else
#define EYEGAZETRACKER_API
#define EYEGAZETRACKER_API_LOCAL
#endif 
#endif

typedef boost::function<void(int, cv::Point2f)> calibstart_callback;
typedef boost::function<void(int, cv::Point2f)> calibstop_callback;
typedef boost::function<void(int, \
							std::vector<cv::Point2f>, \
							std::vector<cv::Point2f>, \
							std::vector<cv::Point2f>, \
							std::vector<bool>, \
							std::vector<bool>)> calibresults_callback;

typedef boost::function<void(rclgaze::OpenAPIDataRecord)> gazedata_callback;

typedef boost::function<void(rclgaze::log_data)> gazelog_callback;

typedef boost::function<void(std::vector<boost::numeric::ublas::matrix<double>>, \
	std::vector<boost::numeric::ublas::matrix<double>>, \
	int, int, int, int)> caliblog_callback;

typedef boost::function<void(std::vector<std::vector<cv::Vec2f>>, \
	std::vector<std::vector<cv::Vec2f>>, \
	std::vector<std::vector<cv::Vec2f>>, \
	std::vector<std::vector<cv::Vec2f>>, \
	int)> headcaliblog_callback;

class EyeTracker
{
public:
	EYEGAZETRACKER_API EyeTracker(std::string settings_directory);
	EYEGAZETRACKER_API ~EyeTracker();
	
	EYEGAZETRACKER_API bool setInputSource(int deviceId, rclgaze::eyeSide side);
	EYEGAZETRACKER_API bool setInputSource(const char* filename, rclgaze::eyeSide side);
	EYEGAZETRACKER_API void removeInputSource(rclgaze::eyeSide side);
	EYEGAZETRACKER_API bool start();
	EYEGAZETRACKER_API void stop();
	EYEGAZETRACKER_API bool getFrame(cv::Mat& ret, rclgaze::eyeSide side);
	EYEGAZETRACKER_API bool getPupil(cv::RotatedRect& retEllipse, rclgaze::eyeSide side);
	EYEGAZETRACKER_API bool getGlints(std::vector<cv::Point2f>& retGlintList, rclgaze::eyeSide side);
	EYEGAZETRACKER_API cv::Vec2f getCalibrationError(rclgaze::eyeSide side);
	EYEGAZETRACKER_API int getCalibrationNumValid(rclgaze::eyeSide side);
	EYEGAZETRACKER_API void startLogging(bool log_data, bool record_video, std::string directory);
	EYEGAZETRACKER_API void stopLogging();
	// Calibration
	EYEGAZETRACKER_API void removeCalibrationTargetData(int targetID);
	EYEGAZETRACKER_API void initCalibration(int numTargets, rclgaze::eyeSide side);
	EYEGAZETRACKER_API bool finalizeCalibration(rclgaze::eyeSide side);
	EYEGAZETRACKER_API void startCalibrating(int targetID, float targetX, float targetY);
	EYEGAZETRACKER_API void startCalibrating(float targetX, float targetY, bool accuracyTest);
	EYEGAZETRACKER_API bool stopCalibrating();

	// Recalibration
	EYEGAZETRACKER_API void initReCalibration(bool clearError = true);
	EYEGAZETRACKER_API void startRecalibration(cv::Point2f target);
	EYEGAZETRACKER_API bool stopReCalibration(cv::Point2f targetPosition, int headIndex);
	EYEGAZETRACKER_API void finalizeReCalibration();
	EYEGAZETRACKER_API bool clearHeadCalib();

	// mapping pupil pog
	EYEGAZETRACKER_API void map_pupilPOG();

	// set glint template
	EYEGAZETRACKER_API void setGlintTemplate(cv::Vec2f gl01, cv::Vec2f gl02, cv::Vec2f gr01, cv::Vec2f gr02);

	// recording
	EYEGAZETRACKER_API void startRecording(std::string directory);
	EYEGAZETRACKER_API void stopRecording();
	EYEGAZETRACKER_API void saveFrame();
	
	bool recordingFlag;
	cv::VideoWriter video;

	EYEGAZETRACKER_API bool isRunning();
	EYEGAZETRACKER_API bool isCalibInitialized();
	EYEGAZETRACKER_API bool isCalibrated();
	EYEGAZETRACKER_API bool isLogging();
	EYEGAZETRACKER_API bool getPOG(cv::Point2f& pog, rclgaze::eyeSide side);
	EYEGAZETRACKER_API float getFPS();
	EYEGAZETRACKER_API int getTestResR();
	EYEGAZETRACKER_API int getTestResL();

	EYEGAZETRACKER_API void registerCalibrationCallbacks(calibstart_callback cb_start_calib, calibstop_callback cb_stop_calib, calibresults_callback cb_calib_results);
	EYEGAZETRACKER_API void registerStreamCallback(gazedata_callback cb);
	EYEGAZETRACKER_API void registerLogCallback(gazelog_callback cb);
	EYEGAZETRACKER_API void registerCalibLogCallback(caliblog_callback cb);
	EYEGAZETRACKER_API void registerHEADCalibLogCallback(headcaliblog_callback cb);
};

#endif