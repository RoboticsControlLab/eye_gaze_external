#ifndef GAZEFILTER_H
#define GAZEFILTER_H
#include "stdafx.h"
// GazeFilter class is a running filter for POG (represented as a cv::Point2f)
// Current filter options:
//   - Simple moving average
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include "EyeGazeCommon.h"
#include "UtilImageProcessing.h"

//#include "DSPFilters/include/DspFilters/Butterworth.h"

enum filterType {
        MOVING_AVERAGE
};
class GazeFilter
{
public:
    GazeFilter(void);
    ~GazeFilter(void);

    // Initialization
    void setWindowSize(int windowSize);
    void setWindowSizs3D(int windowSize);
    void setFilterType(const char* filterTypeStr);
    void setNewPointDistance(int pixDist);
    void setNewPointMax(int pixDist);
    void resetData();
    //
    cv::Point2f filterGaze(cv::Point2f newPOG);
    cv::Point3f filterGaze3D(cv::Point3f newPOG3D);

private:
    int filterWindowSize; // Size of moving average window
    float newPointThres; // Largest pixel distance from previous average before resetting
    float newPointMax; // Maximum value of a data

    filterType currFilterType;
    std::vector<cv::Point2f> data;
    std::vector<cv::Point3f> data3D;
    float* array_POGz;

    //Dsp::SimpleFilter <Dsp::Butterworth::LowPass <6>, 1> butterworth; // create a butterworth low pass filter with maximum order of 6, processing 1 channel of data

    bool isPointInRange(cv::Point2f newPOG); // Checks if point is a close to other points
    bool isPointInRange3D(cv::Point3f newPOG3D);

    bool isDataValid(cv::Point2f newPOG); // Check if data is valid
    bool isDataValid3D(cv::Point3f newPOG3D);
    void convert_to_array ();

        // Different filtering methods
    cv::Point2f movingAverage(cv::Point2f newPOG);
    //cv::Point3f avgButter3D(cv::Point3f newPOG3D);

        // Util??
    cv::Point2f calcAvg();
    cv::Point2f calcAvg3D();
};

#endif
