#ifndef STARBURST_H
#define STARBURST_H
/*!
    @brief Starburst scripts

    These functions are used to perform a starburst search for the pupil edge.
    This algorithm is roughly based on  "Starburst: A hybrid algorithm for video-based eye
    tracking combining feature-baesd and model-based approaches". However, no iterations
    on pupil contour is done, only one pass to find pupil edges. RANSAC can be used afterwards
    to refine feature points.
*/
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <math.h>
#include <ctime>
#include "fstream"

#include "EyeGazeCommon.h"
#include "UtilImageProcessing.h"

#include "stdafx.h"

/*!
 * \brief Main starburst function
 * \param src Original image
 * \param startingPoint Central starting point of starburst rays
 * \param startingAngle Starting search angle, in radians
 * \param angleRange Total angle range to search, in radians
 * \param numRays Number of starburst rays to draw within range
 * \param maxRadius Maximum length of starburst ray
 * \param gradThres Unused
 * \param glintList List of glint centers
 * \param features Return list of features
 * \param edges Return edge image
 * \param prevPupil Reference pupil location
 * \param side Eye side (LEFT_EYE or RIGHT_EYE)
 * \return Returns true if successful (features found)
 */
bool starburst(cv::Mat& src, cv::Point2f startingPoint, double startingAngle, double angleRange,  int numRays, double maxRadius, double gradThres, std::vector<cv::Point2f> glintList, std::vector<cv::Point2f>& features,cv::Mat& edges, cv::RotatedRect prevPupil, rclgaze::eyeSide side);

/*!
 * \brief Create the edge image
 * \param src_filt Original image, filtered
 * \param src_orig Original image
 * \param retEdges Edge image
 * \return Returns true if successful
 */
bool detect_pupil_edge(cv::Mat& src_filt, cv::Mat& retEdges);

/*!
 * \brief Checks for glints by looking farther along starburst ray for large intensity pixels. If the intensity is bright then assume the edge is dur to a glint and return false.
 * \param src Pupil image
 * \param startPoint Ray starting point
 * \param dx Ray direction x-component
 * \param dy Ray direction y-component
 * \param num_to_check Number of pixels to check
 * \return Returns true if there is a glint
 */
bool check_for_glint(cv::Mat& src, cv::Point2f startPoint, float dx, float dy, int num_to_check);

/*!
 * \brief Checks for glints based on distance to candidate glints, if close to a glint then return true
 * \param point Starting ray point
 * \param glintCenterList Vector of candidate glint center points
 * \param distThres Minimum distance from glints in glintCenterList
 * \return Returns true if there is a glint
 */
bool check_for_glint_from_list(cv::Point2f point, std::vector<cv::Point2f> glintCenterList, float distThres) ;

#endif
