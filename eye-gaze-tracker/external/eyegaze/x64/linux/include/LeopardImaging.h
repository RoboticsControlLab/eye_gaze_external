#ifndef LEOPARDIMAGING_H
#define LEOPARDIMAGING_H
#include <string>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <boost/thread.hpp>

//typedef boost::function<void(void*, uint32_t, uint32_t,std::string)> camera_callback;
typedef boost::function<void(void*, uint32_t, uint32_t, uint32_t, const std::string)> camera_callback;

class EYEGAZECAMERA_API_LOCAL LeopardImaging
{
public:
	LeopardImaging();
	~LeopardImaging();

	//void RegisterVideoCallback(void(*image_data_callback)(void * image_data, uint32_t width, uint32_t height, std::string sourcename));
	void registerVideoCallback(camera_callback cb);
	void startCapture();
	void stopCapture();
	bool enumerateCamera(std::string camera_name);
	int getCameras(std::vector<std::string> * camera_list);
	void getFrame(cv::Mat &frame);
private:
	int camera_position;
	int width;
	int height;

	int original_size;

	uint8_t * image_data;
	//DWORD   image_grab_thread;
	//HANDLE image_grab_handle;
	boost::thread image_grab_thread;
	boost::mutex _mutex;
	cv::Mat image_mat;
	unsigned exit_thread;

	int image_grab();
	camera_callback _callback;
	//static DWORD WINAPI image_grab(void* Param);

};

#endif
