#ifndef UTIL_H
#define UTIL_H
#define BOOST_UBLAS_NDEBUG 1

#include "stdafx.h"
#include <boost/math/constants/constants.hpp>
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "EyeGazeCommon.h"
/*!
Contains common enum, struct, const definitions used in eye gaze libs. Not to be used for Qt-related code.

*/
namespace rclgaze {
//	enum calibType {
//		CALIB_2D,
//		CALIB_3D
//	};

	struct stereo_camera_prop {
		cv::Mat cam_left;
		cv::Mat cam_right;
		cv::Mat dist_left;
		cv::Mat dist_right;
		cv::Mat om_mat;
		cv::Mat om_vec;
		cv::Mat T_vec;
		cv::Mat T_dvrk_cam;
		cv::Mat T_cam_dvrk;
		float size_x;
		float size_y;
	};


}

#endif

