#ifndef RANSAC_H
#define RANSAC_H
#include "stdafx.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <math.h>
#include <cmath>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <ctime>
#include "EyeGazeCommon.h"
#include "UtilImageProcessing.h"
#include "UtilVectorMath.h"
#include "boost/timer/timer.hpp"
#include "boost/numeric/ublas/vector.hpp"
#include "boost/math/constants/constants.hpp"

#include "DebugSetting.h"

//#include <QElapsedTimer>
//using namespace boost::numeric;

//#define PI = 3.14159265358979 // <--Is this ok?

/*!
    @brief RANSAC Algorithm for Ellipse Fitting

    These scripts performs RANSAC ellipse fitting on a set of points
    in order to find the ellipse that best fits the set of data.
    Based on "Robust real-time pupil tracking in highly off-axis images"
*/

/*!
 * @namespace ransac
 */
namespace ransac {
     /*!
     * \brief The ellipse struct contains coefficients for the standard and general forms of the ellipse.
     *
     * Standard form (x-x0)^2/a^2 + (y-y0)^2/b^2 = 1
     * General form Q(x,y) = A*x^2 + B*x*y + C*y^2 + D*x + E*y + F = 0
     *
     */
    struct ellipse {
        // Standard form (x-x0)^2/a^2 + (y-y0)^2/b^2 = 1
        double a;
        double b;
        double x0;
        double y0;
        double theta;

        // General form Q(x,y) = A*x^2 + B*x*y + C*y^2 + D*x + E*y + F = 0
        double A;
        double B;
        double C;
        double D;
        double E;
        double F;
    };
    bool ransac_ellipse_fit(cv::Mat src,
                            cv::Mat src_filt,
                            cv::Mat mask,
                            std::vector<cv::Point2f> featureList,
                            float max_area,
                            float min_area,
                            cv::RotatedRect& best_ellipse,
                            int maxIter = 100,
                            double earlyTermThres = 0.95,
                            bool useRefEllipse = false,
                            cv::Point2f refEllipseCenter=cv::Point2f(0,0),
                            rclgaze::eyeSide debug_side=rclgaze::LEFT_EYE);
    bool convert_ellipse_general(cv::RotatedRect src_ellipse, ransac::ellipse& ret_ellipse);
    bool calc_error_of_fit(ransac::ellipse srcEllipse, std::vector<cv::Point2f> points, double alpha, ublas::vector<float>& err);
    bool select_random_features(cv::RNG& rng, std::vector<cv::Point2f>& srcFeatures, std::vector<cv::Point2f>& retFeatureList, double numFeatures);
    bool calc_ellipse_gradient(ransac::ellipse srcEllipse,std::vector<cv::Point2f> points, cv::Mat& retGradx, cv::Mat& retGrady, bool normalize);
    bool get_image_gradient_points(cv::Mat Gx, cv::Mat Gy, std::vector<cv::Point2f> points, cv::Mat& retImGradx, cv::Mat& retImGrady);
    bool check_ellipse_size(cv::RotatedRect ellipse, float minSize, float maxSize, float minRatio, float maxRatio);


}
static std::vector<cv::Point2f> foo;

/*!
 * \brief Fit an ellipse to an OpenCV contour using RANSAC
 * \param src Image containing ellipse to be fit
 * \param src_filt Filtered image containing ellipse to be fit
 * \param mask Mask designating where there is assumed no ellipse
 * \param featureList List of features to fit ellipse to
 * \param best_ellipse Return value of best-fit ellipse
 * \param maxIter Maximum number of RANSAC iterations
 * \param earlyTermThres The percentage of inlier features for early termination of RANSAC iterations
 * \param useRefEllipse Find the ellipse similar to a reference ellipse
 * \param refEllipseCenter Center position of reference ellipse
 * \param side LEFT_EYE or RIGHT_EYE (for debug)
 * \param featSublist Vector of inlier points
 * \param pupilParameter Pupil data (mean, stdev)
 * \return Returns true if successful
 */


// TODO: Fix parameter (featSublist = foo)
// TODO: pupilParameter->make struct instead of a vector since each element represents a different value
bool ransac::ransac_ellipse_fit(cv::Mat src,
                                cv::Mat src_filt,
                                cv::Mat mask,
                                std::vector<cv::Point2f> featureList,
                                float max_area,
                                float min_area,
                                cv::RotatedRect& best_ellipse,
                                int maxIter ,
                                double earlyTermThres  ,
                                bool useRefEllipse ,
                                cv::Point2f refEllipseCenter ,
                                rclgaze::eyeSide side);
/*!
 * \brief Convert OpenCV ellipse to general equation of ellipse, store in ransac::ellipse
 * \param src_ellipse OpenCV ellipse (cv::RotatedRect)
 * \param ret_ellipse Converted ransac::ellipse, with ellipse coefficients calculated
 * \return Returns true if successful
 */
bool ransac::convert_ellipse_general(cv::RotatedRect src_ellipse, ransac::ellipse& ret_ellipse);
/*!
 * \brief Calculate the error from each feature point to the perimeter of ellipse
 * \param srcEllipse Ellipse to measure
 * \param points Vector of feature points
 * \param alpha Error of being one pixel away from ellipse perimeter
 * \param err Return vector of errors
 * \return
 */
bool ransac::calc_error_of_fit(ransac::ellipse srcEllipse, std::vector<cv::Point2f> points, double alpha, ublas::vector<float>& err);
/*!
 * \brief Select a random subset of feature points
 * \param rng OpenCV random number generator to use
 * \param srcFeatures Full list of feature points
 * \param retFeatureList Random subset of srcFeatures
 * \param numFeatures Number of features to include in random subset
 * \return Returns true if successful. Returns false if (size of all feature points) == (size of subset)
 */
bool ransac::select_random_features(cv::RNG& rng, std::vector<cv::Point2f>& srcFeatures, std::vector<cv::Point2f>& retFeatureList, double numFeatures);
/*!
 * \brief Calculate the gradient of the ellipse at each input point
 * \param srcEllipse Ellipse
 * \param points Points at which to calculate the ellipse
 * \param retGradx Vector of ellipse gradient x-component
 * \param retGrady Vector of ellipse gradient y-component
 * \param normalize If true, the ellipse gradient magnitude will be normalized to 1
 * \return Returns true if successful
 */
bool ransac::calc_ellipse_gradient(ransac::ellipse srcEllipse,std::vector<cv::Point2f> points, cv::Mat& retGradx, cv::Mat& retGrady, bool normalize);
/*!
 * \brief Get the image gradient at each input point
 * \param Gx Image gradient x-component
 * \param Gy Image gradient y-component
 * \param points Points at which to get the image gradient
 * \param retImGradx Vector of image gradient x-component
 * \param retImGrady Vector of image gradient y-component
 * \return
 */
bool ransac::get_image_gradient_points(cv::Mat Gx, cv::Mat Gy, std::vector<cv::Point2f> points, cv::Mat& retImGradx, cv::Mat& retImGrady);
/*!
 * \brief Checks if ellipse is within specified size and form factor limits
 * \param ellipse Ellipse to test
 * \param minSize Minimum ellipse size
 * \param maxSize Maximum ellipse size
 * \param minRatio Minimum ellipse size ratio (minor/major)
 * \param maxRatio Maximum ellipse size ratio (minor/major)
 * \return Returns true if ellipse is within the size/form factor limits
 */
bool ransac::check_ellipse_size(cv::RotatedRect ellipse, float minSize, float maxSize, float minRatio, float maxRatio);


#endif // RANSAC_H
