/*!
 @brief Vector math utility functions for Boost UBlas and OpenCV vectors

 */
#ifndef UTILVECTORMATH_H
#define UTILVECTORMATH_H
#include "stdafx.h"
// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

// Boost
// to avoid runtime error for matrix inverse
// http://boost.2283326.n4.nabble.com/boost-ublas-lu-substitute-runtime-error-td3747071.html
#define BOOST_UBLAS_NDEBUG 1

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/operation.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/assert.hpp>

// Qt
//#include <qdebug.h>

// Std
#include <math.h>

using namespace boost::numeric;

#if _WINDOWS
#ifdef UTIL_DLL_EXPORTS
#define UTIL_API __declspec(dllexport)   
#else  
#define UTIL_API __declspec(dllimport)   
#endif
#else
#define UTIL_API 
#endif

namespace rclgaze {

	/*!
	 * \brief Splits the x,y,z channels of srcVector into three cv::Mat column matrices (x,y,z)
	 * \param srcVector
	 * \param x
	 * \param y
	 * \param z
	 * \return Returns true if successful
	 */
	UTIL_API bool splitXYZ(std::vector<cv::Point3f>& srcVector, cv::Mat& x, cv::Mat& y, cv::Mat& z);

	/*!
	 * \brief Splits the x,y,z channels of srcVector into three ublas vectors (x,y,z)
	 * \param srcVector
	 * \param x
	 * \param y
	 * \param z
	 * \return Returns true is successful
	 */
	UTIL_API bool  splitXYZ(std::vector<cv::Point3f>& srcVector, ublas::vector<double>& x, ublas::vector<double>&  y, ublas::vector<double>&  z);

	/*!
	 * \brief Splits the x,y channels of srcVector into two ublas vectors (x,y)
	 * \param srcVector
	 * \param x
	 * \param y
	 * \return Returns true is successful
	 */
	UTIL_API bool  splitXY(std::vector<cv::Point2f>& srcVector, cv::Mat& x, cv::Mat& y);

	/*!
	 * \brief Splits the x,y channels of srcVector into two cv::Mat column matrices (x,y)
	 * \param srcVector
	 * \param x
	 * \param y
	 * \return Returns true if successful
	 */
	UTIL_API bool  splitXY(std::vector<cv::Point2f>& srcVector, ublas::vector<float>& x, ublas::vector<float>& y);

	/*!
	 * \brief Splits the x,y channels of srcVector (int) into two cv::Mat column matrices (x,y)
	 * \param srcVector
	 * \param x
	 * \param y
	 * \return Returns true if successful
	 */
	UTIL_API bool  splitXY(std::vector<cv::Point>& srcVector, cv::Mat& x, cv::Mat& y);

	/*!
	 * \brief Splits the x,y channels of srcVector (int) into two ublas vectors (x,y)
	 * \param srcVector
	 * \param x
	 * \param y
	 * \return Returns true if successful
	 */
	UTIL_API bool  splitXY(std::vector<cv::Point>& srcVector, ublas::vector<float>& x, ublas::vector<float>& y);

	/*!
	 * \brief Calculate the sqrt of each element in a ublas vector
	 * \param srcVector
	 * \return Returns the square root of each element in srcVector
	 */
	UTIL_API ublas::vector<float>  getSqrtElementUblas(const ublas::vector<float>& srcVector);

	/*!
	 * \brief Calculates the absolute value of each element in a ublas vector
	 * \param srcVector
	 * \return Returns the absolute value of each element in srcVector
	 */
	UTIL_API ublas::vector<float>  getAbsUblas(const ublas::vector<float>& srcVector);

	/*!
	 * \brief Calculates the matrix inverse
	 * \param input Input matrix
	 * \param inverse Inverse of input
	 * \return Returns true if successful
	 */
	UTIL_API bool  getInverseMat(const boost::numeric::ublas::matrix<double>& input, boost::numeric::ublas::matrix<double>& inverse);

	/*!
	 * \brief Calculates the average value in a vector
	 * \param input
	 * \return Returns average value in input vector
	 */
	UTIL_API float  getAvg(std::vector<float>& input);

	/*!
	 * \brief Calculates the average value of each channel in a vector
	 * \param input
	 * \return Returns vector containing average values of each channel in input vector
	 */
	UTIL_API cv::Vec3f  getAvgChannel(std::vector<cv::Vec3f>& input);
	/*!
	 * \brief Calculates the sqrt of a scalar
	 * \param x
	 * \return Returns square root of x
	 */
	UTIL_API float  getSqrtScalar(float x);

	/*!
	 * \brief Calculates the absolute value of a scalar
	 * \param x
	 * \return Returns absolute value of x
	 */
	UTIL_API float  getAbsScalar(float x);

	/*!
	 * \brief Converts ublas matrix to OpenCV matrix
	 * \param matIn ublas:matrix<double>
	 * \param matOut cv::Mat
	 */
	UTIL_API void  cvtUblas2CvMat(ublas::matrix<double>& matIn, cv::Mat& matOut);

	/*!
	 * \brief Calculates the polyfit of points, returns vector of cv::Vec2f
	 *
	 * \param oX Source
	 * \param oY Destination
	 * \param nDegree Currently only works for first order
	 * \return Returns <(k_px,b_px),(k_py,b_py)>, where PG = pupil*k + b
	 */
}
#endif // UTILVECTORMATH_H
