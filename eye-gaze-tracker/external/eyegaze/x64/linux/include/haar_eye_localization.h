#ifndef HAAR_EYE_LOCALIZATION_H
#define HAAR_EYE_LOCALIZATION_H
#include "stdafx.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <ctime>
#include "EyeGazeCommon.h"
#include "boost/timer/timer.hpp"


#endif