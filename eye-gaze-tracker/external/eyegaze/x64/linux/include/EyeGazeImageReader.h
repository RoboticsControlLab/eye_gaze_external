#ifndef EYEGAZEIMAGEREADER_H
#define EYEGAZEIMAGEREADER_H


#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "opencv2/videoio.hpp"
#include "opencv2/video.hpp"

#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

//#include "Util.h"
#include "EyeGazeCommon.h"
#include "UtilString.h"
#include "UtilImageProcessing.h"
#include "VideoReader.h"
#include "LeopardImaging.h"


enum EYEGAZECAMERA_API_LOCAL frameState {
	READ,
	UNREAD
};

enum EYEGAZECAMERA_API_LOCAL deviceType {
	CAMERA,
	VIDEO
};
typedef boost::function<void(cv::Mat image)> image_reader_callback;

struct EYEGAZECAMERA_API_LOCAL deviceProp { // Device properties
	deviceType          device;
	char                source_name[100];
	VideoReader    cap;
	LeopardImaging		cam;
	cv::Mat             camMat;   // Camera intrinsic matrix
	std::vector<float>  camDist;  // Camera distortion coefficients
	bool                initFlag; // true if device has been initialized
};

class EYEGAZECAMERA_API_LOCAL EyeGazeImageReader
{
public:
	EyeGazeImageReader();
	~EyeGazeImageReader();

	bool addVideoDevice(const char* filename, rclgaze::eyeSide side);
	bool addCameraDevice(const char* devicename, rclgaze::eyeSide side);

	void resetDevice(rclgaze::eyeSide side);

	bool setCamDistProp(cv::Mat& intrinsic_mat, std::vector<float>& distortion_mat, rclgaze::eyeSide side);
	
	bool start();
	bool stop();

	bool getFrame(cv::Mat& refLeft, cv::Mat& retRight);
	bool isRightEyeActive();
	bool isLeftEyeActive();
	void registerVideoCallback(boost::function<void(cv::Mat)> f1, boost::function<void(cv::Mat)> f2);
	//void registerVideoCallback(void(*image_data_callback_left)(cv::Mat frame), void(*image_data_callback_right)(cv::Mat frame));
private:
	void _setDevice(deviceProp& videodevice, deviceType type, const char* devname);
	bool _initDevice(deviceProp& videodevice);
	deviceProp _right_eye_device;
	deviceProp _left_eye_device;
	cv::Mat _curr_frame_right;
	cv::Mat _curr_frame_left;
	bool _is_stereo;
	bool _left_read;
	bool _right_read;
	
	frameState _frame_state;
	boost::mutex _image_mutex;

	// Camera callback
	image_reader_callback _left_callback;
	image_reader_callback _right_callback;
	bool _is_callback_set;
	void _camera_grabbing(void * image_data, uint32_t width, uint32_t height, uint32_t channels, std::string sourcename);

};

#endif
