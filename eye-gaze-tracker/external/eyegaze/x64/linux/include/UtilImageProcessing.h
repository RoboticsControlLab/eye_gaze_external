#ifndef UTILIMAGEPROCESSING_H
#define UTILIMAGEPROCESSING_H
#include "stdafx.h"
// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

// Qt
//#include <qimage.h>
//#include <QPoint>
//#include <qdebug.h>
//#include <qelapsedtimer.h>

// STD
#include <cmath>
#include <numeric>
#include <boost/math/constants/constants.hpp>

#if _WINDOWS
#ifdef UTIL_DLL_EXPORTS
#define UTIL_API __declspec(dllexport)   
#else  
#define UTIL_API __declspec(dllimport)   
#endif
#else
#define UTIL_API 
#endif

#define USE_CPP_FLOAT

namespace rclgaze {
	const float pi = boost::math::constants::pi<float>();

	/*!
	 @brief Image processing utility functions.

	 Image processing utility functions using OpenCV.
	 */

	 // Ellipses-------------------------------------------------------------------------------------------------
	 /*!
		 @brief Find a single ellipse within the image.

		 An ellipse is found from the input binary image. The contours in the image are found, and an ellipse is fit to the contours.
		 The best fitting ellipse is returned. Returns true if an ellipse has been found.

		 @param src Source binary image
		 @oaram retEllipse Fitted ellipse
		 @param retContour Contour of the fitted ellipse
		 @param minIQ Minimum isoperimetric quotient ratio (1 = circular, 0 = very not circular)
		 @param minArea Minimum pixel area of ellipse
		 @param maxArea Maximum pixel area of ellipse
		 @param convexHull Set as true if convex sections of contours should be removed prior to fitting ellipse
		 @param cutEdges Set as true if the top and bottom sections of all contours should be removed prior to fitting ellipse.

		 @return bool
	 */
	UTIL_API bool findEllipse(cv::Mat src, cv::RotatedRect &retEllipse, std::vector< cv::Point > &retContour, float minIQ, float minArea, float maxArea, bool convexHull, bool cutEdges);

	/*!
		@brief Removes top and bottom sections of ellipse contour. Used to refine ellipse fitting.
		Returns true if succesful.

		@param src Image containing ellipse
		@param contour Contour of ellipse
		@param retContour Fixed contour of ellipse

		@return bool Returns true if successful
	*/
	UTIL_API bool processEllipseContour(cv::Mat src, std::vector< cv::Point > contour, std::vector<cv::Point > &retContour);

	/*!
		@brief Shifts the center of an ellipse (OpenCV RotatedRect).

		@param ellipse
		@param shift Distance (x,y) to shift ellipse by
	*/
	UTIL_API void shiftEllipse(cv::RotatedRect &ellipse, cv::Point shift);

	// Image ROI transformation----------------------------------------------------------------------------------
	/*!
		@brief Set rectangular region of interest

		@param src Original image
		@param dst Return image
		@param size Size of ROI
		@param center Center of ROI
		@param shift The distance that the upper-left hand corner of the image has been shifted by
	*/
	UTIL_API void setROI(cv::Mat src, cv::Mat &dst, cv::Size size, cv::Point center, cv::Point &shift);
	/*!
		@brief Translate all of the points in a contour

		@param contour Contour to translate
		@param shift Distance to translate by
	*/
	UTIL_API void shiftContour(std::vector<cv::Point> &contour, cv::Point shift);

	UTIL_API void shiftPoint(cv::Point2f& point, cv::Point shift);

	// Image transformations-------------------------------------------------------------------------------------
	/*!
		@brief Split cv::Mat image horizontally into two halves.

		@param src Original image
		@param dstLeft Left half of src
		@param dstRight Right half of src
	*/
	UTIL_API bool splitImage(cv::Mat &src, cv::Mat &dstLeft, cv::Mat &dstRight);
	/*!
		@brief Fills an image matrix with a new matrix, making sure image sizes are the same.

		@param oldImage Original image matrix
		@param newImage Image to place in oldImage
		@param newWidth Width of newImage
		@param newHeight Height of newImage

		@return bool Returns true if successful
	*/
	UTIL_API bool fillImageSize(cv::Mat oldImage, cv::Mat& newImage, int newWidth, int newHeight);

	/*!
		@brief Convert BGR image to greyscale

		@param src Original BGR image
		@param dst Final greyscale image
	*/
	UTIL_API void cvtGreyscale(cv::Mat& src, cv::Mat& dst);

	// Point math-------------------------------------------------------------------------------------------------
	/*!
		@brief Find the distance between two cv::Point2f

		@param p1 2D point
		@param p2 2D ponit
	*/
	UTIL_API float getPointDist(cv::Point p1, cv::Point p2);
	/*!
		@brief Finds the distance between two cv::Point3f

		@param p1 3D point
		@param p2 3D point
	*/
	UTIL_API float getPointDist3D(cv::Point2f p1, cv::Point3f p2);

	// Other------------------------------------------------------------------------------------------------------
	///*!
	//    @brief Show the histogram of an image (needs a cv::waitKey() to draw)

	//    @param windowTitle Title of GUI window
	//    @param hist Histogram (cv::calcHist)
	//*/
	//void showHist(const char* windowTitle, cv::Mat hist);
	/*!
		@brief Calculate the image gradient at each point in an image

		@param src Original image (greyscale)
		@param retImGradx Output of image gradient x-component
		@param retImGrady Output of image gradient y-component
		@param retImGradmag Output of image gradient magnitude
		@param normalize If true, normalize gradient magnitude to 1
		@param ksize Soze of Sobel kernel

		@return bool Returns true if successful
	*/
	UTIL_API bool calcImageGradient(cv::Mat src, cv::Mat& retImGradx, cv::Mat& retImGrady, cv::Mat& retImGradmag, bool normalize, int ksize);
	/*!
		@brief Generates a threshold value for pupils based on image mean and stdev

		@param mean Image mean
		@param stdev Image standard deviation

		@return float Returns threshold value
	*/
	UTIL_API float getROIPupilThresVal(float mean, float stdev);
	/*!
		@brief compute image stdDev and mean of image intensity along columns (ps: opencv does it along rows)
		@param src source image
	 *  @return A cv::Scalar scalar of 2 contains mean val and stdDev val, index 0 and 1
	 */
	UTIL_API cv::Scalar getStdDevVertical(cv::Mat src);
	/**
	 * @brief isRotatedRectValid    check if a rotated rectange is valid by size
	 * @param r reference to rotated rect to be checked
	 * @return true if rotated rect is not nully constructed
	 */
	UTIL_API bool isRotatedRectValid(cv::RotatedRect& r);
}
#endif // UTILIMAGEPROCESSING_H
