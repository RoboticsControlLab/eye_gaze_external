#ifndef GAZE3DESTIMATION_H
#define GAZE3DESTIMATION_H
//#include <QString>
//#include <QDebug>
//#include <QFile>
//#include <QDateTime>

#include "stdafx.h"

#include <fstream>
#include <iostream>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/operation.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/triangular.hpp>

#include "UtilVectorMath.h"
#include "Gaze2DEstimation.h"
#include "utilEndoscope.h"



using namespace boost::numeric;


void calc_gaze_3D(cv::Vec2f pogleft, \
                  cv::Vec2f pogright, \
                  ublas::matrix<double>& bleft, \
                  ublas::matrix<double>& bright, \
                  stereo_camera_prop cam_prop, \
                  cv::Vec3f& res);

void lstsq_gaze_3D(std::vector<cv::Point2f> dataRight, \
                   std::vector<cv::Point2f> dataLeft, \
                   std::vector<cv::Point3f> targetPos, \
                   stereo_camera_prop& cam_prop, \
                   ublas::matrix<double>& B_left, \
                   ublas::matrix<double>& B_right);


void project_points(boost::numeric::ublas::matrix<double>& data, \
                    boost::numeric::ublas::matrix<double> cam_int, \
                    boost::numeric::ublas::matrix<double> T_world_cam, \
                    boost::numeric::ublas::vector<double> kc, \
                    double alpha, \
                    std::vector<cv::Point2f>& res);



#endif // GAZE3DESTIMATION_H
