#ifndef VIDEOREADER_H
#define VIDEOREADER_H


#include <string>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <boost/thread.hpp>

typedef boost::function<void(void*, uint32_t, uint32_t,uint32_t, const std::string)> video_callback;

class EYEGAZECAMERA_API_LOCAL VideoReader
{
public:
	VideoReader();
	~VideoReader();

	void registerVideoCallback(video_callback cb);
	void startReading();
	void stopReading();
	bool setVideoSource(std::string video_filename);
	void getFrame(cv::Mat &frame);
private:
	cv::VideoCapture _cap;
	std::string _filename;
	int _width;
	int _height;
	int _fps;
	int _original_size;
	bool _callback_set;

	cv::Mat _image_mat;
	boost::thread _image_read_thread;
	boost::mutex _mutex;
	unsigned _exit_thread;

	int _imageRead();
	video_callback _callback;
};

#endif
