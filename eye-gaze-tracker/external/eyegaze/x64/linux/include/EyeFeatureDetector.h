#ifndef FEATUREDETECTION_H
#define FEATUREDETECTION_H
#include "stdafx.h"
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

// Qt
//#include <QDateTime>
//#include "qdebug.h"

// Std
#include <iostream>
#include <fstream>
#include <math.h>
#include "UtilImageProcessing.h"
// Gaze
#include "PupilDetector.h"
#include "EyeGazeCommon.h"

#include "GuiDebugWindow.h"
#include "GazeFilter.h"
#include "Glints.h"

// Debug
#include "DebugSetting.h"
// Constants
#include "EyeDetectorConstants.h"

//const double NUM_PIX_KEEP = 0.4; // Percentage of pixels to keep for rough pupil
//const double NUM_PIX_KEEP_ROI = 0.1; // Percentage of pixels to keep for rough pupil with ROI

/*!
 * \brief The EyeFeatureDetector class handles the detection of a single eye.
 *
 *  This detector processes images in order to find the pupil and glint locations.
 *  Each frame that is processed contributes to detection in subsequent frames, so
 *  the eye subject should remain the same.
 */
class EyeFeatureDetector {
public:
    /*!
     * \brief EyeFeatureDetector
     */
	EyeFeatureDetector(std::string settings_directory);

	~EyeFeatureDetector();

    void init();
    bool findPupilGlintVector(cv::Mat srcFrame);

    bool getPupil(cv::RotatedRect& retPupil);
    bool getGlintList(std::vector<cv::Point2f>& retGlintList);
    float getGlintDistance(int index0, int index1);
    cv::Vec3f getGlintDistanceAll();
    cv::Point2f getPGVectorZero(float glintRef = -1, int glintIdx = 0);
    std::vector<cv::Point2f> getPGVectorAll(cv::Vec3f glintRef = cv::Vec3f(-1,-1,-1));
   rclgaze::eyeSide getEyeSide();

    void setGlintTemplate(int newDx,int newDy, int newDx_sec, int newDy_sec);
    void setEyeSide(rclgaze::eyeSide side);

private:
	int counter;
    // Member variables--------------------------------------------------------
    // Image
    cv::Mat _raw_frame; // Original image frame
   rclgaze::eyeSide _side;

    // Pupil detection
    cv::CascadeClassifier _pupil_cascade;
    cv::RotatedRect _prev_pupil; //(x,y) coordinates of previous pupil position
    cv::RotatedRect _curr_pupil; //(x,y) coordinates of current pupil position
    float _prev_rough_thres;
    std::vector<float> _pupil_thres;
    bool _prev_pupil_valid;
    bool _curr_pupil_valid;

    // Glint detection
    std::vector<cv::Point2f> _glints;
    std::vector<cv::Point2f> _prev_glints;
    bool _prev_glints_valid;
    bool _curr_glints_valid;
    cv::Point2f _null_glint;
    Glints _glint_template;
    bool _template_set;
    int _num_fail_match;
	std::vector<cv::Vec3f> _prev_glint_dist_list;
	#if (DEBUG_GLINT == true)
		int frame_count_left;
		int frame_count_right;
	#endif

    // train tracker for pupil
    std::vector<cv::RotatedRect> _training_pupil;

    // train tracker for glints
    std::vector<std::vector<cv::Point2f> > _training_glints;
    cv::Point2f _average_training_glints(); // Note: Not used

    // Pupil glint vector
    float _pgx [3];
    float _pgy [3];

    // Other
    GazeFilter _filter_pupil;

    //Counters
    int _frame_count;
    int _num_fail;

    // Member functions--------------------------------------------------------
    void _preprocess(cv::Mat &src, cv::Mat &dst);

    // Glints
	int _glintscaleind;
	std::vector<float> _glintscale;
    bool _sortGlints(std::vector<cv::Point2f> &glintList);
    bool _sortGlintPair(cv::Point2f point, std::vector<cv::Point2f>& candGlint, float dx, float dy, float &minDiffR, float &minDiffTh, int& ind);
    bool _matchGlintsHennessey(std::vector<cv::Point2f>& glintList, cv::Point2f pupilCenter,float pupilWidth);

	// Feature detection functions
    bool _findPupil(cv::Mat image, bool useROI, cv::RotatedRect& retPupil);
    bool _findGlint(cv::Mat image,cv::RotatedRect pupil, std::vector<cv::Point2f> refGlintList, bool useRefGlintList, int numGlints );

};

#endif /* FEATUREDETECTION_H */
