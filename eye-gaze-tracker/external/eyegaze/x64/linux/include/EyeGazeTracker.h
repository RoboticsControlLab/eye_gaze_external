#ifndef GAZETRACKER_H
#define GAZETRACKER_H
//#include "stdafx.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <boost/thread.hpp>
//#include <boost/asio.hpp>
#include <boost/timer/timer.hpp>
#include <boost/numeric/ublas/matrix.hpp>
//#include <boost/regex.hpp>
//#include <boost/lockfree/queue.hpp>
#include <time.h>
#include <iostream>
#include <fstream>
#include <queue>

//#include "GazeDataLogger.h"
//#include "EyeImageReader.h"

#include "../EyeGazeDetectionLib/EyeDetector.h"
#include "../UtilLib/EyeGazeCommon.h"
#include "../EyeGazeDetectionLib/GazeDataFilter.h"
#include "../EyeGazeCameraLib/EyeImageReader.h"
#include "../EyeGazeCalibrationLib/Gaze2DEstimation.h"
#include "../EyeGazeCalibrationLib/Gaze3DEstimation.h"

#if _WINDOWS
#ifdef EYEGAZETRACKER_DLL_EXPORTS
#define EYEGAZETRACKER_API __declspec(dllexport)   
#else  
#define EYEGAZETRACKER_API __declspec(dllimport)   
#endif
#define EYEGAZETRACKER_API_LOCAL
#else
#if __GNUC__ >= 4
#define EYEGAZETRACKER_API __attribute__ ((visibility ("default")))
#define EYEGAZETRACKER_API_LOCAL __attribute__ ((visibility ("hidden")))
#else
#define EYEGAZETRACKER_API
#define EYEGAZETRACKER_API_LOCAL
#endif 
#endif

enum EYEGAZETRACKER_API_LOCAL calib_state {
    PAUSED,
    CALIBRATING,
    FINISHED,
    RECALIBRATING
};

struct EYEGAZETRACKER_API_LOCAL NewEyeFrameData {
	int id;
	uchar* frame;
};

struct EYEGAZETRACKER_API_LOCAL ProcessedEyeFrameData {
	int id;
	cv::Mat frame;
	cv::Point2f pog;
	cv::RotatedRect pupil;
	cv::Vec3f glintDist;
	std::vector<cv::Point2f> glintList;
	std::vector<cv::Point2f> pgList;
	bool eyeFound;
	ProcessedEyeFrameData() : id(0) {}
};

struct EYEGAZETRACKER_API_LOCAL CalibrationTargetData {
	int id;
	cv::Point2f targetPosition;
};

namespace gazetracker {
	typedef boost::function<void(int, cv::Point2f)> calibstart_callback;
	typedef boost::function<void(int, cv::Point2f)> calibstop_callback;
	typedef boost::function<void(int, \
		std::vector<cv::Point2f>, \
		std::vector<cv::Point2f>, \
		std::vector<cv::Point2f>, \
		std::vector<bool>, \
		std::vector<bool>)> calibresults_callback;

	typedef boost::function<void(std::vector<boost::numeric::ublas::matrix<double>>, \
								 std::vector<boost::numeric::ublas::matrix<double>>, \
								 int, int, int, int)> caliblog_callback;
	typedef boost::function<void(rclgaze::OpenAPIDataRecord)> gazedata_callback;

	typedef boost::function<void(rclgaze::log_data)> gazelog_callback;

	typedef boost::function<void(std::vector<std::vector<cv::Vec2f>>, \
								std::vector<std::vector<cv::Vec2f>>, \
								std::vector<std::vector<cv::Vec2f>>, \
								std::vector<std::vector<cv::Vec2f>>, \
								int)> headcaliblog_callback;
}

class EYEGAZETRACKER_API_LOCAL EyeGazeTracker  {
public:
    EyeGazeTracker(std::string settings_directory);
    ~EyeGazeTracker();

	bool setInputSource(int deviceId, rclgaze::eyeSide side);
	bool setInputSource(const char* filename, rclgaze::eyeSide side);
	void removeInputSource(rclgaze::eyeSide side);
	bool start();
	void stop();
	bool getFrame(cv::Mat& ret, rclgaze::eyeSide side);
	bool getPupil(cv::RotatedRect& retEllipse, rclgaze::eyeSide side);
	bool getGlints(std::vector<cv::Point2f>& retGlintList, rclgaze::eyeSide side);
	cv::Vec2f getCalibrationError(rclgaze::eyeSide side);
	int getCalibrationNumValid(rclgaze::eyeSide side);
	void startLogging(bool log_data, bool record_video, std::string directory);
	void stopLogging();
	bool isLogging();
	// Calibration
	void removeCalibrationTargetData(int targetID);
	void initCalibration(int numTargets, rclgaze::eyeSide side);
	bool finalizeCalibration(rclgaze::eyeSide side);
	void startCalibrating(int targetID, float targetX, float targetY);
	void startCalibrating(float targetX, float targetY, bool accuracyTest);
	bool stopCalibrating();

	// Recalibration
	void initReCalibration(bool clearError = true);
	bool stopReCalibration(cv::Point2f targetPosition, int headIndex);
	void startRecalibration(cv::Point2f target);
	void finalizeReCalibration();

	// mapping pupil pog
	void map_pupilPOG();

	// set glint template
	void setGlintTemplate(cv::Vec2f gl01, cv::Vec2f gl02, cv::Vec2f gr01, cv::Vec2f gr02);

	// recording
	void startRecording(std::string directory);
	void stopRecording();
	void _recordVideo(std::string filename); 
	void saveFrame();
	cv::VideoWriter _video;
	boost::thread _vidthr;
	std::queue<cv::Mat> _frame_write_queue;


	bool isRunning();
	bool isCalibInitialized();
	bool isCalibrated();

	bool getPOG(cv::Point2f& pog, rclgaze::eyeSide side);
	int getTestResR();
	int getTestResL();
	double fps;
	std::vector<double> fpsVec;
	// void setLogStatus(bool status);

	stereo_camera_prop endoProp;

	float getFPS();

	void registerCalibrationCallback(gazetracker::calibstart_callback c1, gazetracker::calibstop_callback c2, gazetracker::calibresults_callback c3);
	void registerStreamCallbacks(gazetracker::gazedata_callback c);
	void registerLogCallback(gazetracker::gazelog_callback c);
	void registerCalibLogCallback(gazetracker::caliblog_callback c);
	void registerHEADCalibLogCallback(gazetracker::headcaliblog_callback c);
private:
	int testcounter;
	int testcounter2;

	gazetracker::gazedata_callback _data_cb;
	gazetracker::calibstart_callback _calib_start_cb;
	gazetracker::calibstop_callback _calib_stop_cb;
	gazetracker::calibresults_callback _calib_results_cb;
	gazetracker::gazelog_callback _log_cb;
	gazetracker::caliblog_callback _caliblog_cb;
	gazetracker::headcaliblog_callback _headcaliblog_cb;

	bool _is_stream_callback_set;
	bool _is_calib_callback_set;
	bool _is_log_callback_set;
	bool _is_calib_log_callback_set;
	bool _is_head_calib_log_callback_set;
		// Member variables
	float					_fps;
    bool				_run_flag;
    bool				_calib_initialized_flag;
    bool                _calibrated_flag;
    calib_state         _calib_state_left;
    calib_state         _calib_state_right;
	rclgaze::calibType          _calib_type;
    boost::mutex         _calib_state_mutex;

    std::vector<bool> _calib_valid_left;
    std::vector<cv::Point2f> _calib_pog_left;
    std::vector<bool> _calib_valid_right;
    std::vector<cv::Point2f> _calib_pog_right;

    boost::mutex        _run_state_mutex;
    cv::Point3f*        _robot_pos;
    cv::Matx33f*        _robot_rot;
    cv::Vec3f           _glint_dist_right;
    cv::Vec3f           _glint_dist_left;
	rclgaze::eyeSide             _calib_side;
    boost::thread		_thr;
    boost::mutex		_image_mutex; // Shared mutex allows for single writer/multiple readers
    cv::Point2f			_pog_right;
    cv::Point2f			_pog_left;
	cv::Point2f			_pog_best;
    cv::Vec3f           _pog_3D;
    bool                _pog_right_found;
    bool                _pog_left_found;
    int                 _glint_index_right;
    int                 _glint_index_left;
    int                 _glint_num;

	// Unused
    std::vector<float>  _robot_pose;
    std::vector<cv::Point2f> _proj_pt;

	// For calculation of average POG
	bool _prev_pog_best_valid;
	bool _prev_pog_left_valid;
	bool _prev_pog_right_valid;
	cv::Point2f _prev_pog_best;
	cv::Point2f _prev_pog_left;
	cv::Point2f _prev_pog_right;
	    // Time
    long long _time_tick;
    float _time;

    //GazeDataLogger      _logger;
    bool _is_logging;

	bool _recordingFlag;
    //clientSocket *_socket;

    // Image device setup******************************************************
    EyeImageReader    _input_reader;
	void _receiveNewFrameRight(cv::Mat frame);
	void _receiveNewFrameLeft(cv::Mat frame);

	void _processFrame(std::queue<NewEyeFrameData>& input_queue, std::queue<ProcessedEyeFrameData>& output_queue, EyeDetector& detector);
	 

	//std::queue<NewEyeFrameData> _left_raw_frame_queue;
	//std::queue<NewEyeFrameData> _right_raw_frame_queue;
	//std::queue<ProcessedEyeFrameData> _left_output_frame_queue;
	//std::queue<ProcessedEyeFrameData> _right_output_frame_queue;
	//boost::mutex _mutex_left_raw_queue;
	//boost::mutex _mutex_right_raw_queue;
	//boost::mutex _mutex_left_output_queue;
	//boost::mutex _mutex_right_output_queue;

    // Image processing
	boost::mutex _mutex_left_frame;
	boost::mutex _mutex_right_frame;
    cv::Mat				_curr_frame_right;
    cv::Mat				_curr_frame_left;
    cv::Mat             _curr_frame_right_copy;
    cv::Mat             _curr_frame_left_copy;
	std::vector<cv::Point2f> _curr_glint_list_right;
	cv::RotatedRect _curr_pupil_right;
	std::vector<cv::Point2f> _curr_glint_list_left;
	cv::RotatedRect _curr_pupil_left;
    EyeDetector*	_eye_detector_right;
    EyeDetector*	_eye_detector_left;
    GazeDataFilter			_filter_left;
    GazeDataFilter			_filter_right;


    // Calibration


    int _num_targets;
    //	int currTarget;
    cv::Point2f _curr_target;
    std::vector<std::vector<cv::Point2f>> _calib_pg_data_right;       //contains PG0, PG1, PG2, truncated for G0,1,2
    std::vector<std::vector<cv::Point2f>> _calib_pg_data_left;        //contains PG0, PG1, PG2, truncated for G0,1,2
    std::vector<cv::Point2f> _calib_pupil_data_right;
    std::vector<cv::Point2f> _calib_pupil_data_left;

    std::vector<cv::Vec3f> _calib_glint_dist_right;
    std::vector<cv::Vec3f> _calib_glint_dist_left;
	cv::Vec2f _calib_err_right;
	cv::Vec2f _calib_err_left;
	void _calculateCalibResults(std::vector<cv::Point2f> calibPGData, \
                                            std::vector<cv::Point2f> targetData, \
                                            boost::numeric::ublas::matrix<double> transmat, \
                                            std::vector<bool>& calibValid, \
                                            std::vector<cv::Point2f>& calibPOGData, \
											cv::Vec2f& err) ;


    std::vector<cv::Point2f> _target_pos;
    std::vector<cv::Point2f> _target_pos_l;
    std::vector<cv::Point2f> _target_pos_r;

    std::vector<boost::numeric::ublas::matrix<double> > _trans_mat_right;
    std::vector<boost::numeric::ublas::matrix<double> > _trans_mat_left;
    std::vector<boost::numeric::ublas::matrix<double> > _trans_mat_right_orig;      // prevent ReCalibration error propogates
    std::vector<boost::numeric::ublas::matrix<double> > _trans_mat_left_orig;


    void _setCalibState(calib_state state, rclgaze::eyeSide side);

    bool _calculateCalibrationMatrix(rclgaze::eyeSide side, \
                                    std::vector<std::vector<cv::Point2f>>& calib_pg_data, \
                                    std::vector<cv::Point2f>& calib_pupil_data, \
                                    std::vector<std::vector<cv::Point2f>>& target_vector, \
                                    int& main_glint, \
                                    int& sec_glint, \
                                    cv::Vec3f& glintdist, \
                                    std::vector<cv::Vec3f>& calib_glint_dist, \
                                    std::vector<std::vector<cv::Vec2f>>&  pupil_pg_mapping, \
                                    std::vector<boost::numeric::ublas::matrix<double>>& transmat, \
                                    std::vector<boost::numeric::ublas::matrix<double>>& transmat_orig, \
                                    calib_state& calibstate) ;
    // 3D Calibration
    std::vector<std::vector<cv::Point3f>> _target_pos_3D;
    std::vector<std::vector<cv::Matx33f>> _target_rot_3D;

    // Pupil-PG mapping
    std::vector<std::vector<cv::Vec2f>>   _pupil_pg_mapping_right;
    std::vector<std::vector<cv::Vec2f>>   _pupil_pg_mapping_left;
    // pupil-POG mapping
    //std::vector<std::vector<cv::Point2f>> _pupil_pog_right;
    //std::vector<std::vector<cv::Point2f>> _pupil_pog_left;
    std::vector<std::vector<cv::Point2f>> _pg_error_right;  // from head motion, structure is 3 cols of <left,right,up,down> for G0,G1 and G3
    std::vector<std::vector<cv::Point2f>> _pg_error_left;
    std::vector<std::vector<cv::Point2f>> _pog_error_right;  // from head motion, structure is 3 cols of <left,right,up,down> for G0,G1 and G3
    std::vector<std::vector<cv::Point2f>> _pog_error_left;
    std::vector<std::vector<cv::Vec2f>>   _pg_pog_mapping_right;
    std::vector<std::vector<cv::Vec2f>>   _pg_pog_mapping_left;
    cv::Point2f                           _correction_right = cv::Point2f(0,0);
    cv::Point2f                           _correction_left = cv::Point2f(0,0);



    // Member functions
    void _run();
	void _streamData(double time, long time_tick, long frame_count,\
		cv::Point2f leftpog, cv::Point2f rightpog, cv::Point2f bestpog, \
		bool leftvalid, bool rightvalid, bool bestvalid, \
		bool leftfound, bool rightfound, \
		cv::RotatedRect leftpupil, cv::RotatedRect rightPupil, \
		float leftscale, float rightscale);
	void _logData(double time, long timetick, long frame_count, \
		cv::Point2f leftpog, cv::Point2f rightpog, \
		std::vector<cv::Point2f> leftpg, std::vector<cv::Point2f> rightpg, \
		cv::RotatedRect leftpupil, cv::RotatedRect rightpupil, \
		std::vector<cv::Point2f> leftglints, std::vector<cv::Point2f> rightglints, \
		bool leftvalid, bool rightvalid, \
		bool leftfound, bool rightfound, \
		float calib_x, float calib_y, bool calib_valid);

    void _calcPG(cv::Mat eyeImage,EyeDetector& detector,ProcessedEyeFrameData& result, cv::Vec3f& glint_dist);
	void _calcPOG(rclgaze::eyeSide side, calib_state state, \
		ProcessedEyeFrameData& results, \
		std::vector<std::vector<cv::Point2f>>& calibPGData, \
		std::vector<cv::Point2f>& calibPupilData, \
		std::vector<cv::Vec3f>& calibGlintDist, \
		std::vector<std::vector<cv::Point2f> >& targetVector, \
		cv::Point2f& POG
	);
	bool _calculate_pg_pog_error(int mainGlint, int glintNum, int headIndex, cv::Point2f targetPosition,
		std::vector<boost::numeric::ublas::matrix<double>>& transmat, \
		std::vector<std::vector<cv::Point2f>>& calib_pg_data, std::vector<cv::Point2f>& calib_pupil_data, \
		std::vector<std::vector<cv::Vec2f>>& pupil_pg_mapping, std::vector<std::vector<cv::Point2f>>& pg_error, \
		std::vector<std::vector<cv::Point2f>>& pog_error);
    int _main_glint_right;
    int _sec_glint_right;
    int _main_glint_left;
    int _sec_glint_left;
    //    std::vector<std::vector<cv::Point2f>> targetVector; //a currTarget vector syncronized with pg data
    std::vector<std::vector<cv::Point2f> > _target_vector_left; //a currTarget vector syncronized with pg data
    std::vector<std::vector<cv::Point2f> > _target_vector_right; //a currTarget vector syncronized with pg data

    //QElapsedTimer _fps_timer;
    void _initEndoscopeProp(stereo_camera_prop prop);
    void _saveEndoscopeProp();
    void _initGazeCamProp();

    // recalibration
    //    void recalibration();
    std::vector<cv::Point2f> _recalib;
    int _gripper_cnt;
	bool _pog_best_found;


};




#endif /* GAZETRACKER_H */
