#ifndef GAZEDATAFILTER_H
#define GAZEDATAFILTER_H

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

class EYEGAZEDETECTION_API GazeDataFilter
{
public:
	GazeDataFilter();
	~GazeDataFilter();

	// Initialization
	void setWindowSize(int windowSize);
	void setFilterType(const char* filterTypeStr);
	void setNewPointDistance(int pixDist);
	void setNewPointMax(int pixDist);
	void resetData();
	//
	cv::Point2f filterGaze(cv::Point2f newPOG);
};

#endif
