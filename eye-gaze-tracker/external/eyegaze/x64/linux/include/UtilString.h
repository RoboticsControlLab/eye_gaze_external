#ifndef UTILSTRING_H
#define UTILSTRING_H
#include "stdafx.h"
#include <cstring>
#include <vector>

#if _WINDOWS
#ifdef UTIL_DLL_EXPORTS
#define UTIL_API __declspec(dllexport)   
#else  
#define UTIL_API __declspec(dllimport)   
#endif
#else
#define UTIL_API 
#endif

namespace rclgaze {
	UTIL_API bool checkEndWith(const char* inputStr, const char* endStr);
}
#endif // utilstring_h
